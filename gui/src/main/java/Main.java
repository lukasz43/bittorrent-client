import com.turn.ttorrent.client.Client;
import com.turn.ttorrent.client.ConnectionHandler;
import com.turn.ttorrent.client.IncomingConnectionListener;
import com.turn.ttorrent.client.SharedTorrent;
import com.turn.ttorrent.client.peer.SharingPeer;
import com.turn.ttorrent.common.Torrent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class Main {

    private static final Logger logger =
            LoggerFactory.getLogger(Torrent.class);

    public static List peersList = new ArrayList();

    public static void main(String[] args) {
        final GuiTorrent guiTorrent = new GuiTorrent();
        guiTorrent.prepareFrame();

        prepareClientStart(guiTorrent);
    }

    private static void prepareClientStart(final GuiTorrent guiTorrent) {
        guiTorrent.getStartStopButton().addActionListener(new ActionListener() {

            Client client;

            @Override
            public void actionPerformed(ActionEvent e) {

                if (guiTorrent.getStartStopButton().getLabel().equals("Start")) {

                    guiTorrent.getStartStopButton().setLabel("Stop");

                } else {

                    guiTorrent.getStartStopButton().setLabel("Start");

                }
                if(guiTorrent.getPath() != null && guiTorrent.getDirectory() != null) {
                    if (guiTorrent.getStartStopButton().getLabel().equals("Stop")) {

                        try {
                            client = new Client(
                                    InetAddress.getLocalHost(),
                                    SharedTorrent.fromFile(
                                            new File(guiTorrent.getPath()),
                                            new File(guiTorrent.getDirectory())));
                                            //new File(guiTorrent.getPath()), new File(guiTorrent.getDirectory())));
                        } catch (IOException e1) {

                            e1.printStackTrace();

                        } catch (NoSuchAlgorithmException e1) {
                            e1.printStackTrace();

                        }

                        client.addObserver(new Observer() {
                            @Override
                            public void update(Observable observable, Object data) {
                                Client client = (Client) observable;
                                int progress = (int) client.getTorrent().getCompletion();
                                logger.info("  Progress: {}%", progress);
                                if(progress == 100){
                                    guiTorrent.getStartStopButton().setLabel("Start - pobieranie ukonczone - wybierz nowy plik");
                                    //guiTorrent.getPeerList().setText(guiTorrent.getPeerList().getText() + "\n" + "Pobieranie ukończone pomyślnie");
                                }
                                List<SharingPeer> list = new ArrayList<SharingPeer>(client.getPeers());
                                guiTorrent.setPeersList(list);
                                guiTorrent.setProgressBar(Integer.toString(progress));
                                //guiTorrent.setPeersList(lister);
                                //guiTorrent.setProgressBar(Integer.toString(progress_temp));
                            }
                        });

                        client.setMaxDownloadRate(50.0);
                        client.setMaxUploadRate(50.0);
                        client.download();
                        //client.waitForCompletion();

                    } else {

                        client.stop();
                    }
                }
            }

        });
    }

}
