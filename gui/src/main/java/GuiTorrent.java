import com.turn.ttorrent.client.peer.Rate;
import com.turn.ttorrent.client.peer.SharingPeer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.*;

public class GuiTorrent {
    private Frame mainFrame;
    private Label statusLabel;
    private Button addFileButton;
    private Button startStopButton;
    private Label progressBar;
    private JProgressBar jProgressBar;
    private TextArea peerList;

    private String path;
    private String directory;

    public void setProgressBar(String progress) {
        progressBar.setText(progress + "%");
        jProgressBar.setValue(Integer.parseInt(progress));
    }

    public void setPeersList(java.util.List<SharingPeer> list) {
        String peers = "Peers:\n";
        for(int i =0; i < list.size(); i++){
            String ip = list.get(i).getIp();
            int port = list.get(i).getPort();
            float dlRate = 0.0f;
            float upRate = 0.0f;
            if(list.get(i).getDLRate() != null){
               dlRate = list.get(i).getDLRate().get();
            }
            if(list.get(i).getULRate() != null) {
                upRate = list.get(i).getULRate().get();
            }
            if(dlRate != 0.0f){
                peers += "Ip: " + ip + " Port: " + port + " Download Rate: " + dlRate + " Upload Rate: " + upRate + "\n";
            }

        }
        peerList.setText(peers);
    }

    public TextArea getPeerList() {
        return peerList;
    }

    public String getPath() {
        return path;
    }

    public String getDirectory() {
        return directory;
    }

    public Button getStartStopButton() {
        return startStopButton;
    }

    public void prepareFrame() {
        mainFrame = new Frame("Java Torrent");
        mainFrame.setSize(800, 600);
        mainFrame.setLayout(new GridLayout(5, 1));
        mainFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });

        prepareStatusLabel();
        prepareProgressBar();
        prepareFileButton();
        prepareStartButton();
        preparePeerList();

        mainFrame.add(addFileButton);
        mainFrame.add(startStopButton);
        mainFrame.add(statusLabel);
        mainFrame.add(jProgressBar);
        mainFrame.add(peerList);
        mainFrame.setVisible(true);
    }

    private void preparePeerList(){
        peerList = new TextArea();
        peerList.setVisible(true);
        peerList.setText("Peers:");
    }

    private void prepareProgressBar() {
        progressBar = new Label();
        progressBar.setVisible(true);
        progressBar.setText("0%");

        jProgressBar = new JProgressBar();
        jProgressBar.setValue(0);
        jProgressBar.setStringPainted(true);
    }

    private void prepareStartButton() {
        startStopButton = new Button("Start");
    }

    private void prepareStatusLabel() {
        statusLabel = new Label();
        statusLabel.setAlignment(Label.CENTER);
        statusLabel.setSize(350, 100);
        statusLabel.setText("File Selected : none");
    }

    private void prepareFileButton() {
        final FileDialog fileDialog = new FileDialog(mainFrame, "Select file");

        addFileButton = new Button("Open File");
        addFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileDialog.setVisible(true);
                path = fileDialog.getDirectory() + fileDialog.getFile();
                directory = fileDialog.getDirectory();
                if (path != null) {
                    statusLabel.setText("File Selected :" + path);
                } else {
                    statusLabel.setText("File Selected : none");
                }
            }
        });
    }

}
