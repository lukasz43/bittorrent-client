/**
 * Copyright (C) 2011-2012 Turn, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.turn.ttorrent.client;

import com.turn.ttorrent.client.announce.Announce;
import com.turn.ttorrent.client.announce.AnnounceException;
import com.turn.ttorrent.client.announce.AnnounceResponseListener;
import com.turn.ttorrent.client.peer.PeerActivityListener;
import com.turn.ttorrent.client.peer.SharingPeer;
import com.turn.ttorrent.common.Peer;
import com.turn.ttorrent.common.Torrent;
import com.turn.ttorrent.common.protocol.PeerMessage;
import com.turn.ttorrent.common.protocol.TrackerMessage;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.BitSet;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Client extends Observable implements Runnable,
	AnnounceResponseListener, IncomingConnectionListener, PeerActivityListener {

	private static final Logger logger =
		LoggerFactory.getLogger(Client.class);


	private static final int UNCHOKING_FREQUENCY = 3;

	private static final int OPTIMISTIC_UNCHOKE_ITERATIONS = 3;

	private static final int RATE_COMPUTATION_ITERATIONS = 2;
	private static final int MAX_DOWNLOADERS_UNCHOKE = 4;

	public enum ClientState {
		WAITING,
		VALIDATING,
		SHARING,
		SEEDING,
		ERROR,
		DONE;
	};

	private static final String BITTORRENT_ID_PREFIX = "-TO0042-";

	private SharedTorrent torrent;
	private ClientState state;
	private Peer self;

	private Thread thread;
	private boolean stop;
	private long seed;

	private ConnectionHandler service;
	private Announce announce;
	private ConcurrentMap<String, SharingPeer> peers;
	private ConcurrentMap<String, SharingPeer> connected;

	private Random random;

	public Client(InetAddress address, SharedTorrent torrent)
		throws UnknownHostException, IOException {
		this.torrent = torrent;
		this.state = ClientState.WAITING;

		String id = Client.BITTORRENT_ID_PREFIX + UUID.randomUUID()
			.toString().split("-")[4];

		this.service = new ConnectionHandler(this.torrent, id, address);
		this.service.register(this);

		this.self = new Peer(
			this.service.getSocketAddress()
				.getAddress().getHostAddress(),
			this.service.getSocketAddress().getPort(),
			ByteBuffer.wrap(id.getBytes(Torrent.BYTE_ENCODING)));

		this.announce = new Announce(this.torrent, this.self);
		this.announce.register(this);

		logger.info("BitTorrent client [{}] for {} started and " +
			"listening at {}:{}...",
			new Object[] {
				this.self.getShortHexPeerId(),
				this.torrent.getName(),
				this.self.getIp(),
				this.self.getPort()
			});

		this.peers = new ConcurrentHashMap<String, SharingPeer>();
		this.connected = new ConcurrentHashMap<String, SharingPeer>();
		this.random = new Random(System.currentTimeMillis());
	}

	public void setMaxDownloadRate(double rate) {
		this.torrent.setMaxDownloadRate(rate);
	}

	public void setMaxUploadRate(double rate) {
		this.torrent.setMaxUploadRate(rate);
	}

	public Peer getPeerSpec() {
		return this.self;
	}

	public SharedTorrent getTorrent() {
		return this.torrent;
	}

	public Set<SharingPeer> getPeers() {
		return new HashSet<SharingPeer>(this.peers.values());
	}

	private synchronized void setState(ClientState state) {
		if (this.state != state) {
			this.setChanged();
		}
		this.state = state;
		this.notifyObservers(this.state);
	}

	public ClientState getState() {
		return this.state;
	}

	public void download() {
		this.share(0);
	}

	public void share() {
		this.share(-1);
	}

	public synchronized void share(int seed) {
		this.seed = seed;
		this.stop = false;

		if (this.thread == null || !this.thread.isAlive()) {
			this.thread = new Thread(this);
			this.thread.setName("bt-client(" +
				this.self.getShortHexPeerId() + ")");
			this.thread.start();
		}
	}

	public void stop() {
		this.stop(true);
	}

	public void stop(boolean wait) {
		this.stop = true;

		if (this.thread != null && this.thread.isAlive()) {
			this.thread.interrupt();
			if (wait) {
				this.waitForCompletion();
			}
		}

		this.thread = null;
	}

	public void waitForCompletion() {
		if (this.thread != null && this.thread.isAlive()) {
			try {
				this.thread.join();
			} catch (InterruptedException ie) {
				logger.error(ie.getMessage(), ie);
			}
		}
	}

	public boolean isSeed() {
		return this.torrent.isComplete();
	}

	@Override
	public void run() {
		// First, analyze the torrent's local data.
		try {
			this.setState(ClientState.VALIDATING);
			this.torrent.init();
		} catch (IOException ioe) {
			logger.warn("Error while initializing torrent data: {}!",
				ioe.getMessage(), ioe);
		} catch (InterruptedException ie) {
			logger.warn("Client was interrupted during initialization. " +
					"Aborting right away.");
		} finally {
			if (!this.torrent.isInitialized()) {
				try {
					this.service.close();
				} catch (IOException ioe) {
					logger.warn("Error while releasing bound channel: {}!",
						ioe.getMessage(), ioe);
				}

				this.setState(ClientState.ERROR);
				this.torrent.close();
				return;
			}
		}

		if (this.torrent.isComplete()) {
			this.seed();
		} else {
			this.setState(ClientState.SHARING);
		}

		if (this.stop) {
			logger.info("Download is complete and no seeding was requested.");
			this.finish();
			return;
		}

		this.announce.start();
		this.service.start();

		int optimisticIterations = 0;
		int rateComputationIterations = 0;

		while (!this.stop) {
			optimisticIterations =
				(optimisticIterations == 0 ?
				 Client.OPTIMISTIC_UNCHOKE_ITERATIONS :
				 optimisticIterations - 1);

			rateComputationIterations =
				(rateComputationIterations == 0 ?
				 Client.RATE_COMPUTATION_ITERATIONS :
				 rateComputationIterations - 1);

			try {
				this.unchokePeers(optimisticIterations == 0);
				this.info();
				if (rateComputationIterations == 0) {
					this.resetPeerRates();
				}
			} catch (Exception e) {
				logger.error("An exception occurred during the BitTorrent " +
						"client main loop execution!", e);
			}

			try {
				Thread.sleep(Client.UNCHOKING_FREQUENCY*1000);
			} catch (InterruptedException ie) {
				logger.trace("BitTorrent main loop interrupted.");
			}
		}

		logger.debug("Stopping BitTorrent client connection service " +
				"and announce threads...");

		this.service.stop();
		try {
			this.service.close();
		} catch (IOException ioe) {
			logger.warn("Error while releasing bound channel: {}!",
				ioe.getMessage(), ioe);
		}

		this.announce.stop();

		logger.debug("Closing all remaining peer connections...");
		for (SharingPeer peer : this.connected.values()) {
			peer.unbind(true);
		}

		this.finish();
	}

	private void finish() {
		this.torrent.close();

		if (this.torrent.isFinished()) {
			this.setState(ClientState.DONE);
		} else {
			this.setState(ClientState.ERROR);
		}

		logger.info("BitTorrent client signing off.");
	}

	public synchronized void info() {
		float dl = 0;
		float ul = 0;
		for (SharingPeer peer : this.connected.values()) {
			dl += peer.getDLRate().get();
			ul += peer.getULRate().get();
		}

		logger.info("{} {}/{} pieces ({}%) [{}/{}] with {}/{} peers at {}/{} kB/s.",
			new Object[] {
				this.getState().name(),
				this.torrent.getCompletedPieces().cardinality(),
				this.torrent.getPieceCount(),
				String.format("%.2f", this.torrent.getCompletion()),
				this.torrent.getAvailablePieces().cardinality(),
				this.torrent.getRequestedPieces().cardinality(),
				this.connected.size(),
				this.peers.size(),
				String.format("%.2f", dl/1024.0),
				String.format("%.2f", ul/1024.0),
			});
		for (SharingPeer peer : this.connected.values()) {
			Piece piece = peer.getRequestedPiece();
			logger.debug("  | {} {}",
				peer,
				piece != null
					? "(downloading " + piece + ")"
					: ""
			);
		}
	}


	private synchronized void resetPeerRates() {
		for (SharingPeer peer : this.connected.values()) {
			peer.getDLRate().reset();
			peer.getULRate().reset();
		}
	}


	private SharingPeer getOrCreatePeer(Peer search) {
		SharingPeer peer;

		synchronized (this.peers) {
			logger.trace("Searching for {}...", search);
			if (search.hasPeerId()) {
				peer = this.peers.get(search.getHexPeerId());
				if (peer != null) {
					logger.trace("Found peer (by peer ID): {}.", peer);
					this.peers.put(peer.getHostIdentifier(), peer);
					this.peers.put(search.getHostIdentifier(), peer);
					return peer;
				}
			}

			peer = this.peers.get(search.getHostIdentifier());
			if (peer != null) {
				if (search.hasPeerId()) {
					logger.trace("Recording peer ID {} for {}.",
						search.getHexPeerId(), peer);
					peer.setPeerId(search.getPeerId());
					this.peers.put(search.getHexPeerId(), peer);
				}

				logger.debug("Found peer (by host ID): {}.", peer);
				return peer;
			}

			peer = new SharingPeer(search.getIp(), search.getPort(),
				search.getPeerId(), this.torrent);
			logger.trace("Created new peer: {}.", peer);

			this.peers.put(peer.getHostIdentifier(), peer);
			if (peer.hasPeerId()) {
				this.peers.put(peer.getHexPeerId(), peer);
			}

			return peer;
		}
	}


	private Comparator<SharingPeer> getPeerRateComparator() {
		if (ClientState.SHARING.equals(this.state)) {
			return new SharingPeer.DLRateComparator();
		} else if (ClientState.SEEDING.equals(this.state)) {
			return new SharingPeer.ULRateComparator();
		} else {
			throw new IllegalStateException("Client is neither sharing nor " +
					"seeding, we shouldn't be comparing peers at this point.");
		}
	}


	private synchronized void unchokePeers(boolean optimistic) {

		TreeSet<SharingPeer> bound = new TreeSet<SharingPeer>(
				this.getPeerRateComparator());
		bound.addAll(this.connected.values());

		if (bound.size() == 0) {
			logger.trace("No connected peers, skipping unchoking.");
			return;
		} else {
			logger.trace("Running unchokePeers() on {} connected peers.",
				bound.size());
		}

		int downloaders = 0;
		Set<SharingPeer> choked = new HashSet<SharingPeer>();

		for (SharingPeer peer : bound.descendingSet()) {
			if (downloaders < Client.MAX_DOWNLOADERS_UNCHOKE) {
				if (peer.isChoking()) {
					if (peer.isInterested()) {
						downloaders++;
					}

					peer.unchoke();
				}
			} else {
				choked.add(peer);
			}
		}

		if (choked.size() > 0) {
			SharingPeer randomPeer = choked.toArray(
					new SharingPeer[0])[this.random.nextInt(choked.size())];

			for (SharingPeer peer : choked) {
				if (optimistic && peer == randomPeer) {
					logger.debug("Optimistic unchoke of {}.", peer);
					peer.unchoke();
					continue;
				}

				peer.choke();
			}
		}
	}


	@Override
	public void handleAnnounceResponse(int interval, int complete,
		int incomplete) {
		this.announce.setInterval(interval);
	}

	@Override
	public void handleDiscoveredPeers(List<Peer> peers) {
		if (peers == null || peers.isEmpty()) {

			return;
		}

		logger.info("Got {} peer(s) in tracker response.", peers.size());

		if (!this.service.isAlive()) {
			logger.warn("Connection handler service is not available.");
			return;
		}

		for (Peer peer : peers) {

			SharingPeer match = this.getOrCreatePeer(peer);
			if (this.isSeed()) {
				continue;
			}

			synchronized (match) {
				if (!match.isConnected()) {
					this.service.connect(match);
				}
			}
		}
	}



	@Override
	public void handleNewPeerConnection(SocketChannel channel, byte[] peerId) {
		Peer search = new Peer(
			channel.socket().getInetAddress().getHostAddress(),
			channel.socket().getPort(),
			(peerId != null
				? ByteBuffer.wrap(peerId)
				: (ByteBuffer)null));

		logger.info("Handling new peer connection with {}...", search);
		SharingPeer peer = this.getOrCreatePeer(search);

		try {
			synchronized (peer) {
				if (peer.isConnected()) {
					logger.info("Already connected with {}, closing link.",
						peer);
					channel.close();
					return;
				}

				peer.register(this);
				peer.bind(channel);
			}

			this.connected.put(peer.getHexPeerId(), peer);
			peer.register(this.torrent);
			logger.debug("New peer connection with {} [{}/{}].",
				new Object[] {
					peer,
					this.connected.size(),
					this.peers.size()
				});
		} catch (Exception e) {
			this.connected.remove(peer.getHexPeerId());
			logger.warn("Could not handle new peer connection " +
					"with {}: {}", peer, e.getMessage());
		}
	}


	@Override
	public void handleFailedConnection(SharingPeer peer, Throwable cause) {
		logger.warn("Could not connect to {}: {}.", peer, cause.getMessage());
		this.peers.remove(peer.getHostIdentifier());
		if (peer.hasPeerId()) {
			this.peers.remove(peer.getHexPeerId());
		}
	}

	@Override
	public void handlePeerChoked(SharingPeer peer) { /* Do nothing */ }

	@Override
	public void handlePeerReady(SharingPeer peer) { /* Do nothing */ }

	@Override
	public void handlePieceAvailability(SharingPeer peer,
			Piece piece) { /* Do nothing */ }

	@Override
	public void handleBitfieldAvailability(SharingPeer peer,
			BitSet availablePieces) { /* Do nothing */ }

	@Override
	public void handlePieceSent(SharingPeer peer,
			Piece piece) { /* Do nothing */ }


	@Override
	public void handlePieceCompleted(SharingPeer peer, Piece piece)
		throws IOException {
		synchronized (this.torrent) {
			if (piece.isValid()) {

				this.torrent.markCompleted(piece);
				logger.debug("Completed download of {} from {}. " +
					"We now have {}/{} pieces",
					new Object[] {
						piece,
						peer,
						this.torrent.getCompletedPieces().cardinality(),
						this.torrent.getPieceCount()
					});

				PeerMessage have = PeerMessage.HaveMessage.craft(piece.getIndex());
				for (SharingPeer remote : this.connected.values()) {
					remote.send(have);
				}

				this.setChanged();
				this.notifyObservers(this.state);
			} else {
				logger.warn("Downloaded piece#{} from {} was not valid ;-(",
					piece.getIndex(), peer);
			}

			if (this.torrent.isComplete()) {
				logger.info("Last piece validated and completed, finishing download...");

				for (SharingPeer remote : this.connected.values()) {
					if (remote.isDownloading()) {
						int requests = remote.cancelPendingRequests().size();
						logger.info("Cancelled {} remaining pending requests on {}.",
							requests, remote);
					}
				}

				this.torrent.finish();

				try {
					this.announce.getCurrentTrackerClient()
						.announce(TrackerMessage
							.AnnounceRequestMessage
							.RequestEvent.COMPLETED, true);
				} catch (AnnounceException ae) {
					logger.warn("Error announcing completion event to " +
						"tracker: {}", ae.getMessage());
				}

				logger.info("Download is complete and finalized.");
				this.seed();
			}
		}
	}

	@Override
	public void handlePeerDisconnected(SharingPeer peer) {
		if (this.connected.remove(peer.hasPeerId()
					? peer.getHexPeerId()
					: peer.getHostIdentifier()) != null) {
			logger.debug("Peer {} disconnected, [{}/{}].",
				new Object[] {
					peer,
					this.connected.size(),
					this.peers.size()
				});
		}

		peer.reset();
	}

	@Override
	public void handleIOException(SharingPeer peer, IOException ioe) {
		logger.warn("I/O error while exchanging data with {}, " +
			"closing connection with it!", peer, ioe.getMessage());
		peer.unbind(true);
	}



	private synchronized void seed() {
		// Silently ignore if we're already seeding.
		if (ClientState.SEEDING.equals(this.getState())) {
			return;
		}

		logger.info("Download of {} pieces completed.",
			this.torrent.getPieceCount());

		this.setState(ClientState.SEEDING);
		if (this.seed < 0) {
			logger.info("Seeding indefinetely...");
			return;
		}


		logger.info("Seeding for {} seconds...", this.seed);
		Timer timer = new Timer();
		timer.schedule(new ClientShutdown(this, timer), this.seed*1000);
	}


	public static class ClientShutdown extends TimerTask {

		private final Client client;
		private final Timer timer;

		public ClientShutdown(Client client, Timer timer) {
			this.client = client;
			this.timer = timer;
		}

		@Override
		public void run() {
			this.client.stop();
			if (this.timer != null) {
				this.timer.cancel();
			}
		}
	};
}
