/**
 * Copyright (C) 2011-2012 Turn, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.turn.ttorrent.client.announce;

import com.turn.ttorrent.client.SharedTorrent;
import com.turn.ttorrent.common.Peer;
import com.turn.ttorrent.common.protocol.TrackerMessage;
import com.turn.ttorrent.common.protocol.TrackerMessage.*;

import java.net.URI;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class TrackerClient {

	private final Set<AnnounceResponseListener> listeners;

	protected final SharedTorrent torrent;
	protected final Peer peer;
	protected final URI tracker;

	public TrackerClient(SharedTorrent torrent, Peer peer, URI tracker) {
		this.listeners = new HashSet<AnnounceResponseListener>();
		this.torrent = torrent;
		this.peer = peer;
		this.tracker = tracker;
	}

	public void register(AnnounceResponseListener listener) {
		this.listeners.add(listener);
	}

	public URI getTrackerURI() {
		return this.tracker;
	}

	public abstract void announce(AnnounceRequestMessage.RequestEvent event,
		boolean inhibitEvent) throws AnnounceException;


	protected void close() {

	}

	protected String formatAnnounceEvent(
		AnnounceRequestMessage.RequestEvent event) {
		return AnnounceRequestMessage.RequestEvent.NONE.equals(event)
			? ""
			: String.format(" %s", event.name());
	}

	protected void handleTrackerAnnounceResponse(TrackerMessage message,
		boolean inhibitEvents) throws AnnounceException {
		if (message instanceof ErrorMessage) {
			ErrorMessage error = (ErrorMessage)message;
			throw new AnnounceException(error.getReason());
		}

		if (! (message instanceof AnnounceResponseMessage)) {
			throw new AnnounceException("Unexpected tracker message type " +
				message.getType().name() + "!");
		}

		if (inhibitEvents) {
			return;
		}

		AnnounceResponseMessage response =
			(AnnounceResponseMessage)message;
		this.fireAnnounceResponseEvent(
			response.getComplete(),
			response.getIncomplete(),
			response.getInterval());
		this.fireDiscoveredPeersEvent(
			response.getPeers());
	}

	protected void fireAnnounceResponseEvent(int complete, int incomplete,
		int interval) {
		for (AnnounceResponseListener listener : this.listeners) {
			listener.handleAnnounceResponse(interval, complete, incomplete);
		}
	}


	protected void fireDiscoveredPeersEvent(List<Peer> peers) {
		for (AnnounceResponseListener listener : this.listeners) {
			listener.handleDiscoveredPeers(peers);
		}
	}
}
