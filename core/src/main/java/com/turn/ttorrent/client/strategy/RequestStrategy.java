package com.turn.ttorrent.client.strategy;

import java.util.BitSet;
import java.util.SortedSet;

import com.turn.ttorrent.client.Piece;

public interface RequestStrategy {

	Piece choosePiece(SortedSet<Piece> rarest, BitSet interesting, Piece[] pieces);
}
