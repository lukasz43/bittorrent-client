/**
 * Copyright (C) 2011-2012 Turn, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.turn.ttorrent.client.peer;

import java.io.Serializable;
import java.util.Comparator;

public class Rate implements Comparable<Rate> {

	public static final Comparator<Rate> RATE_COMPARATOR =
		new RateComparator();

	private long bytes = 0;
	private long reset = 0;
	private long last = 0;

	public synchronized void add(long count) {
		this.bytes += count;
		if (this.reset == 0) {
			this.reset = System.currentTimeMillis();
		}
		this.last = System.currentTimeMillis();
	}

	public synchronized float get() {
		if (this.last - this.reset == 0) {
			return 0;
		}

		return this.bytes / ((this.last - this.reset) / 1000.0f);
	}

	public synchronized void reset() {
		this.bytes = 0;
		this.reset = System.currentTimeMillis();
		this.last = this.reset;
	}

	@Override
	public int compareTo(Rate other) {
		return RATE_COMPARATOR.compare(this, other);
	}

	private static class RateComparator
			implements Comparator<Rate>, Serializable {

		private static final long serialVersionUID = 72460233003600L;

		@Override
		public int compare(Rate a, Rate b) {
			if (a.get() > b.get()) {
				return 1;
			}

			return -1;
		}
	}
}
