/**
 * Copyright (C) 2011-2012 Turn, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.turn.ttorrent.tracker;

import com.turn.ttorrent.bcodec.BEValue;
import com.turn.ttorrent.common.Peer;
import com.turn.ttorrent.common.Torrent;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * A BitTorrent tracker peer.
 *
 * <p>
 * Represents a peer exchanging on a given torrent. In this implementation,
 * we don't really care about the status of the peers and how much they
 * have downloaded / exchanged because we are not a torrent exchange and
 * don't need to keep track of what peers are doing while they're
 * downloading. We only care about when they start, and when they are done.
 * </p>
 *
 * <p>
 * We also never expire peers automatically. Unless peers send a STOPPED
 * announce request, they remain as long as the torrent object they are a
 * part of.
 * </p>
 */
public class TrackedPeer extends Peer {

	private static final Logger logger =
		LoggerFactory.getLogger(TrackedPeer.class);

	private static final int FRESH_TIME_SECONDS = 30;

	private long uploaded;
	private long downloaded;
	private long left;
	private Torrent torrent;

	public enum PeerState {
		UNKNOWN,
		STARTED,
		COMPLETED,
		STOPPED;
	};

	private PeerState state;
	private Date lastAnnounce;


	public TrackedPeer(Torrent torrent, String ip, int port,
			ByteBuffer peerId) {
		super(ip, port, peerId);
		this.torrent = torrent;


		this.state = PeerState.UNKNOWN;
		this.lastAnnounce = null;

		this.uploaded = 0;
		this.downloaded = 0;
		this.left = 0;
	}


	public void update(PeerState state, long uploaded, long downloaded,
			long left) {
		if (PeerState.STARTED.equals(state) && left == 0) {
			state = PeerState.COMPLETED;
		}

		if (!state.equals(this.state)) {
			logger.info("Peer {} {} download of {}.",
				new Object[] {
					this,
					state.name().toLowerCase(),
					this.torrent,
				});
		}

		this.state = state;
		this.lastAnnounce = new Date();
		this.uploaded = uploaded;
		this.downloaded = downloaded;
		this.left = left;
	}


	public boolean isCompleted() {
		return PeerState.COMPLETED.equals(this.state);
	}


	public long getUploaded() {
		return this.uploaded;
	}

	public long getDownloaded() {
		return this.downloaded;
	}

	public long getLeft() {
		return this.left;
	}

	public boolean isFresh() {
		return (this.lastAnnounce != null &&
				(this.lastAnnounce.getTime() + (FRESH_TIME_SECONDS * 1000) >
				 new Date().getTime()));
	}

	public BEValue toBEValue() throws UnsupportedEncodingException {
		Map<String, BEValue> peer = new HashMap<String, BEValue>();
		if (this.hasPeerId()) {
			peer.put("peer id", new BEValue(this.getPeerId().array()));
		}
		peer.put("ip", new BEValue(this.getIp(), Torrent.BYTE_ENCODING));
		peer.put("port", new BEValue(this.getPort()));
		return new BEValue(peer);
	}
}
